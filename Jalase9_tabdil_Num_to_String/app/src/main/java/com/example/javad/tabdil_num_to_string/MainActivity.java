package com.example.javad.tabdil_num_to_string;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final String[] yekan = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
        final String[] dahgan = {"", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};
        final String[] dahtabist = {"ten","eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
        final String[] sadgan = {"", "one hundred", "two hundred", "three hundred", "four hundred", "five hundred", "six hundred", "seven hundred", "eight hundred", "nine hundred"};

        Button button = (Button) findViewById(R.id.btn1);
        final EditText editText = (EditText) findViewById(R.id.et1);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int yek, dah, sad;
                int indx = Integer.parseInt(editText.getText().toString());
                if (indx > 1000) {
                    Toast.makeText(MainActivity.this, "try again", Toast.LENGTH_SHORT).show();
                    editText.setText("");
                } else {
                    sad = (int) Math.ceil(indx / 100);
                    int temp = (int) Math.ceil(indx % 100);
                    dah = (int) Math.ceil(temp / 10);
                    yek = (int) Math.ceil(temp % 10);


                    if (indx < 10) {
                        Toast.makeText(MainActivity.this, yekan[yek], Toast.LENGTH_SHORT).show();
                    } else if (temp > 9 && temp < 20 && indx<20) {
                        Toast.makeText(MainActivity.this, dahtabist[temp - 10], Toast.LENGTH_SHORT).show();
                    }
                    else if (temp > 9 && temp < 20 && indx > 99) {
                        Toast.makeText(MainActivity.this, sadgan[sad] + " " + dahtabist[temp - 10], Toast.LENGTH_SHORT).show();
                    }
                    else if (yek != 0) {
                        Toast.makeText(MainActivity.this, sadgan[sad] + " " + dahgan[dah] + " " + yekan[yek], Toast.LENGTH_SHORT).show();
                    } else
                    {
                        Toast.makeText(MainActivity.this, sadgan[sad] + " " + dahgan[dah], Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }
}
